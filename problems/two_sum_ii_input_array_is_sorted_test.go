package problems

import (
	"reflect"
	"testing"
)

func Test_twoSumII(t *testing.T) {
	type args struct {
		numbers []int
		target  int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{"t1", args{numbers: []int{2, 7, 11, 15}, target: 9}, []int{1, 2}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := twoSumII(tt.args.numbers, tt.args.target); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("twoSumII() = %v, want %v", got, tt.want)
			}
		})
	}
}
