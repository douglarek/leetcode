package problems

import (
	"testing"
)

func Test_longestCommonPrefix(t *testing.T) {
	type args struct {
		strs []string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"t1", args{strs: []string{"flower", "flow", "flight"}}, "fl"},
		{"t2", args{strs: []string{"dog", "racecar", "car"}}, ""},
		{"t3", args{strs: []string{"c", "c"}}, "c"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := longestCommonPrefix(tt.args.strs); got != tt.want {
				t.Errorf("longestCommonPrefix() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_longestCommonPrefix0(t *testing.T) {
	type args struct {
		strs []string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"t1", args{strs: []string{"flower", "flow", "flight"}}, "fl"},
		{"t2", args{strs: []string{"dog", "racecar", "car"}}, ""},
		{"t3", args{strs: []string{"c", "c"}}, "c"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := longestCommonPrefix0(tt.args.strs); got != tt.want {
				t.Errorf("longestCommonPrefix0() = %v, want %v", got, tt.want)
			}
		})
	}
}
