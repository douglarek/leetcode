package problems

func dominantIndex(nums []int) int {
	n := len(nums)
	if n < 1 || n > 50 {
		return -1
	}
	var max, maxIndex int
	for i, v := range nums {
		if v > max {
			max = v
			maxIndex = i
		}
	}
	if max < 0 /*|| max > 99*/ {
		return -1
	}
	for _, v := range nums {
		if v == max || v == 0 {
			continue
		}
		if max/v < 2 {
			return -1
		}
	}
	return maxIndex
}
