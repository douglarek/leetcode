package problems

func minSubArrayLen(s int, nums []int) int {
	var i, j, sum, min int
	for j < len(nums) {
		sum += nums[j]
		j++
		for sum >= s {
			if min == 0 || min > j-i {
				min = j - i
			}
			sum -= nums[i]
			i++
		}
	}
	return min
}
