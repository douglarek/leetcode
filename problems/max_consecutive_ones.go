package problems

func findMaxConsecutiveOnes(nums []int) int {
	var i, j int
	for _, v := range nums {
		if v != 1 {
			i = 0
		} else {
			i++
			if i > j {
				j = i
			}
		}
	}
	return j
}
