package problems

import (
	"strings"
)

func longestCommonPrefix(strs []string) string {
	if len(strs) == 0 {
		return ""
	}
	if len(strs) == 1 {
		return strs[0]
	}
	fst := strs[0]
	var carry string
L0:
	for i := range fst {
		t := fst[0 : i+1]
		for _, v := range strs {
			if !strings.HasPrefix(v, t) {
				break L0
			}

		}
		if len(t) > len(carry) {
			carry = t
		}
	}
	return carry
}

// leetcode horizontal scanning
func longestCommonPrefix0(strs []string) string {
	if len(strs) == 0 {
		return ""
	}
	prefix := strs[0]
	for i := 1; i < len(strs); i++ {
		for !strings.HasPrefix(strs[i], prefix) {
			prefix = prefix[0 : len(prefix)-1]
			if prefix == "" {
				return ""
			}
		}
	}
	return prefix
}
