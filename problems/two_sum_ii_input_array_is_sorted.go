package problems

func twoSumII(numbers []int, target int) []int {
	n := len(numbers)
	if n < 2 {
		return nil
	}
	i, j := 0, n-1
	for i < j {
		v := numbers[i] + numbers[j]
		if v == target {
			return []int{i + 1, j + 1}
		} else if v > target {
			j--
		} else {
			i++
		}
	}
	return nil
}
