package problems

func plusOne(digits []int) []int {
	n := len(digits)
	if n == 0 {
		return nil
	}
	digits[n-1]++
	for i := range digits {
		v := digits[n-i-1]
		if v > 9 && i != n-1 {
			digits[n-i-2]++
			digits[n-i-1] = 0
		}
	}
	var result []int
	if digits[0] > 9 {
		result = []int{1, 0}
		for i := 1; i < n; i++ {
			result = append(result, digits[i])
		}
		return result
	}
	return digits
}
