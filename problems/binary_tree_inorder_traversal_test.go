package problems

import (
	"reflect"
	"testing"
)

func Test_inOrderTraversal(t *testing.T) {
	type args struct {
		root *TreeNode
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{"t1", args{root: nil}, nil},
		{"t2", args{root: &TreeNode{
			Val:   0,
			Left:  nil,
			Right: nil,
		}}, []int{0}},
		{"t3", args{root: &TreeNode{
			Val:  1,
			Left: nil,
			Right: &TreeNode{
				Val: 2,
				Left: &TreeNode{
					Val:   3,
					Left:  nil,
					Right: nil,
				},
				Right: nil,
			},
		}}, []int{1, 3, 2}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := inOrderTraversal(tt.args.root); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("inOrderTraversal() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_inOrderTraversalV1(t *testing.T) {
	type args struct {
		root *TreeNode
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{"t1", args{root: nil}, nil},
		{"t2", args{root: &TreeNode{
			Val:   0,
			Left:  nil,
			Right: nil,
		}}, []int{0}},
		{"t3", args{root: &TreeNode{
			Val:  1,
			Left: nil,
			Right: &TreeNode{
				Val: 2,
				Left: &TreeNode{
					Val:   3,
					Left:  nil,
					Right: nil,
				},
				Right: nil,
			},
		}}, []int{1, 3, 2}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := inOrderTraversalV1(tt.args.root); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("inOrderTraversalV1() = %v, want %v", got, tt.want)
			}
		})
	}
}
