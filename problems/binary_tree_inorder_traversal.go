package problems

// TreeNode defines a binary tree node.
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

// inOrderTraversal 中序遍历, 指先访问左(右)子树, 然后访问根, 最后访问右(左)子树的遍历方式.
// 递归方式
func inOrderTraversal(root *TreeNode) []int {
	var a []int

	if root == nil {
		return nil
	}

	if root.Left != nil {
		a = append(a, inOrderTraversal(root.Left)...)
	}
	a = append(a, root.Val)
	if root.Right != nil {
		a = append(a, inOrderTraversal(root.Right)...)
	}

	return a
}

// 迭代方式
func inOrderTraversalV1(root *TreeNode) []int {
	var a []int
	var s []*TreeNode // 使用 slice 模拟栈

	curr := root

	for curr != nil || len(s) != 0 {
		for curr != nil {
			s = append(s, curr)
			curr = curr.Left
		}
		curr = s[len(s)-1]
		s = s[:len(s)-1] // 从栈里面 pop
		a = append(a, curr.Val)
		curr = curr.Right
	}
	return a
}
