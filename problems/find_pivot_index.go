package problems

func pivotIndex(nums []int) int {
	n := len(nums)

	if n <= 0 || n > 10000 {
		return -1
	}

	var sumLeft, sumRight int
	var left, right []int

	for i := range nums {

		sumLeft += nums[i]
		left = append(left, sumLeft)
		sumRight += nums[n-1-i]
		right = append(right, sumRight)
	}

	for i := range left {
		if left[i] == right[n-1-i] {
			return i
		}
	}

	return -1
}
