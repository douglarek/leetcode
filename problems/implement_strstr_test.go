package problems

import "testing"

func Test_strStr(t *testing.T) {
	type args struct {
		haystack string
		needle   string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"t1", args{haystack: "hello", needle: "ll"}, 2},
		{"t2", args{haystack: "aaaaa", needle: "bba"}, -1},
		{"t3", args{haystack: "aaaaa", needle: "a"}, 0},
		{"t4", args{haystack: "aaaaa", needle: "aaa"}, 0},
		{"t5", args{haystack: "aaaaa", needle: "aaaaaa"}, -1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := strStr(tt.args.haystack, tt.args.needle); got != tt.want {
				t.Errorf("strStr() = %v, want %v", got, tt.want)
			}
		})
	}
}
