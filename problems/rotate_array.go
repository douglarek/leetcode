package problems

func rotate(nums []int, k int) {
	k %= len(nums)
	reverse(nums, 0, len(nums)-1) // reverse all
	reverse(nums, 0, k-1)         // reverse top k
	reverse(nums, k, len(nums)-1) // reverse k to end
}

func reverse(nums []int, start, end int) {
	for start < end {
		nums[start], nums[end] = nums[end], nums[start]
		start++
		end--
	}
}

func rotate0(nums []int, k int) {
	n := len(nums)
	if n == 0 || n == 1 {
		return
	}
	for i := 0; i < k; i++ {
		var t []int
		t = append(t, nums[n-1])
		t = append(t, nums[0:n-1]...)
		copy(nums, t)
	}
}
