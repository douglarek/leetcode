package problems

// firstMissingPositive 第一种解法(不符合空间复杂度, 可以得到启发)
func firstMissingPositiveV1(nums []int) int {
	n := len(nums)
	m := make(map[int]int, n+1)
	for i := 1; i <= n+1; i++ {
		m[i] = -1
	}

	for i, v := range nums {
		if _, ok := m[v]; ok {
			m[v] = i
		}
	}
	for i := 1; i <= n+1; i++ {
		if m[i] == -1 {
			return i
		}
	}

	return 1
}
