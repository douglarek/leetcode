package problems

import "testing"

func Test_firstMissingPositiveV1(t *testing.T) {
	type args struct {
		nums []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "t1",
			args: args{nums: []int{3, -1, 0, 1}},
			want: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := firstMissingPositiveV1(tt.args.nums); got != tt.want {
				t.Errorf("firstMissingPositiveV1() = %v, want %v", got, tt.want)
			}
		})
	}
}
