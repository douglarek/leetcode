package problems

func moveZeroes(nums []int) {
	var j int // 记录最左边 0 的位置
	for i, v := range nums {
		if v != 0 {
			nums[i], nums[j] = nums[j], nums[i]
			j++
		}
	}
}
