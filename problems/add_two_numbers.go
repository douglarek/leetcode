package problems

type ListNode struct {
	Val  int
	Next *ListNode
}

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	forword := 0
	result := &ListNode{}
	cur := result
	for l1 != nil || l2 != nil {
		if l1 != nil {
			forword += l1.Val
		}
		if l2 != nil {
			forword += l2.Val
		}

		cur.Next = &ListNode{Val: forword % 10}
		forword = forword / 10
		cur = cur.Next

		if l1 != nil {
			l1 = l1.Next
		}
		if l2 != nil {
			l2 = l2.Next
		}
	}
	if forword != 0 {
		cur.Next = &ListNode{Val: forword}
	}

	return result.Next
}
