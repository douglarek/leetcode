package problems

func reverseString(s string) string {
	a := []rune(s)
	i := 0
	j := len(a) - 1
	for i < j {
		a[i], a[j] = a[j], a[i]
		i++
		j--
	}
	return string(a)
}
