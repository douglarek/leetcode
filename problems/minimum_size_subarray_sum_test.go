package problems

import "testing"

func Test_minSubArrayLen(t *testing.T) {
	type args struct {
		s    int
		nums []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"t1", args{s: 7, nums: []int{2, 3, 1, 2, 4, 3}}, 2},
		{"t2", args{s: 4, nums: []int{1, 4, 4}}, 1},
		{"t3", args{s: 11, nums: []int{1, 2, 3, 4, 5}}, 3},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := minSubArrayLen(tt.args.s, tt.args.nums); got != tt.want {
				t.Errorf("minSubArrayLen() = %v, want %v", got, tt.want)
			}
		})
	}
}
