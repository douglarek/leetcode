package problems

import (
	"strconv"
	"strings"
)

func addBinary(a string, b string) string {
	i, j, carry := len(a)-1, len(b)-1, 0
	var s strings.Builder
	for i >= 0 || j >= 0 {
		sum := carry
		if i >= 0 {
			sum += int(a[i] - '0')
			i--
		}
		if j >= 0 {
			sum += int(b[j] - '0')
			j--
		}
		s.WriteString(strconv.Itoa(sum % 2)) // nolint: gas
		carry = sum / 2
	}

	if carry != 0 {
		s.WriteString(strconv.Itoa(carry)) // nolint: gas
	}

	r := []rune(s.String())
	for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}
	return string(r)
}
